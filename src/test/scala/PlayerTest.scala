import org.scalatest.FunSuite

class PlayerTest extends FunSuite with Player {

  val player1 = 1
  val player2 = 2
  val player3 = 3
  val vx, vy, radius, y = 1f
  val x1 = 100f
  val x2 = 200f
  val x3 = 300f

  val e3 = Entity(3, player3, radius, x3, y, vx, vy)


  test("in collision scope with non moving entities") {
    val e1 = Entity(1, player1, 1f, 100f, 50, 0, 0)
    val e2 = Entity(2, player2, 1f, 200f, 50, 0, 0)
    assert(!inCollisionScope(e1, e2))
  }

  test("in collision scope with moving entities") {
    val e1 = Entity(1, player1, 1f, 100f, 50, 1, 0)
    val e2 = Entity(2, player2, 1f, 200f, 50, 0, 0)
    assert(inCollisionScope(e1, e2))
  }

  test("in collision scope with moving entities but not the right direction") {
    val e1 = Entity(1, player1, 1f, 100f, 50, -1, 0)
    val e2 = Entity(2, player2, 1f, 200f, 50, 0, 0)
    assert(!inCollisionScope(e1, e2))
  }

  test("Attacker should attack if there is no obstacle before chasee") {
    val e1 = Entity(1, player1, 10f, 30, 30, 0, 0)
    val e2 = Entity(2, -1, 4f, 30, 60, 0, 0)
    assert(None != Chaser.handle(List(e1, e2), e1))
  }

  test("Position is not mirrored") {
    assert(789 == mirror(801, 5, 800))
    assert(15 == mirror(-5, 5, 800))
    assert(400 == mirror(400, 2, 800))
  }

  test("speed is reversed when exceeding positive border") {
    val e1 = Entity(1, player1, 10f, 790, 30, 6, 0)
    assert(e1.vx == nextPosition(e1).vx * -1)
  }

  test("speed is reversed when exceeding negative border") {
    val e1 = Entity(1, player1, 5f, 10, 30, -50, 0)
    assert(e1.vx == nextPosition(e1).vx * -1)
  }

  test("next position") {
    val e1 = Entity(1, player1, 3f, 20, 0, 20, 0)
    assert(e1.x + e1.vx == nextPosition(e1).x)
  }

  test("next speed") {
    val e1 = Entity(1, player1, 3f, 20, 20, 0, 0)
    val speed: E = propel(e1, Vector2(1, 0))
    assert(speed.vy == 0)
    assert(speed.vx > 0)
  }

  test("propelled border") {
    val e1 = Entity(1, player1, 5f, 10, 10, 0, 0)
    val big: Entity = propel(e1, Vector2(0, 1))
    val small: Entity = computeDroplet(big, e1)
    assert(collision(big, small))
  }

  test("detect collision") {
    val e1 = Entity(1, player1, 3f, 20, 20, 50, 50)
    val e2 = Entity(2, player2, 3f, 40, 40, 0, 0)
    assert(collision(e1, e2))
  }

  test("self collision scope") {
    val e2 = Entity(2, player2, 1f, 6, 10, 0, 0)
    assert(!inCollisionScope(e2, e2))
  }

  test("propel vector") {
    val e1 = Entity(1, player1, 10f, 20, 20, 0, 0)
    val e2 = Entity(2, player2, 10f, 40, 20, 0, 0)
    assertResult(Vector2(1, 0).normalize)(propelVector(e1, e2).get.normalize)
  }

}
