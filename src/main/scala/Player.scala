

trait Player {
  val zeroVector = Vector2(0, 0)
  val width = 800
  val height = 515
  val relativeSpeed = -200f
  val smallRadius = Math.sqrt(1 / 15f).toFloat
  val bigRadius = Math.sqrt(14 / 15f).toFloat
  val initialVector = Vector2(1, 0)
  lazy val allAngles = (0 until 360).filter(_ % 3 == 0).map(initialVector.rotate)

  case class Entity(id: Int, playerId: Int, radius: Float, x: Float, y: Float, vx: Float, vy: Float)

  case class Vector2(x: Float, y: Float) {
    lazy val magnitudeSquare: Float = squareSum(x, y).toFloat
    lazy val magnitude: Float = Math.sqrt(magnitudeSquare).toFloat
    lazy val normalize = if (magnitude == 0) zeroVector else Vector2(x / magnitude, y / magnitude)

    def scale(d: Float) = Vector2(d * x, d * y)

    def +(v: Vector2) = Vector2(x + v.x, y + v.y)

    def -(v: Vector2) = Vector2(x - v.x, y - v.y)

    def *(v: Vector2) = x * v.x + y * v.y

    def negate = Vector2(-x, -y)

    def rotate(angleInDegree: Int) = {
      val radians: Double = Math.toRadians(angleInDegree)
      val ca = Math.cos(radians).toFloat
      val sa = Math.sin(radians).toFloat
      Vector2((x * ca) - (y * sa), (x * sa) + (y * ca))
    }
  }

  object Vector2 {
    def apply(pair: (Float, Float)): Vector2 = Vector2(pair._1, pair._2)

    def apply(start: E, end: E): Vector2 = Vector2(end.x - start.x, end.y - start.y)
  }

  type E = Entity
  type Es = List[E]
  type V = Vector2

  case class Point(x: Float, y: Float)

  object Point {
    def apply(pair: (Float, Float)): Point = Point(pair._1, pair._2)
  }

  def entitiesOfPlayer(entities: Es, playerId: Int): Es = entities.filter(_.playerId == playerId)

  def notEntity(entity: E): (E) => Boolean = _.id != entity.id

  def p2pDistance(e1: E, e2: E) = sqrtOfSquareSum(e1.x - e2.x, e1.y - e2.y)

  def sqrtOfSquareSum(x: Float, y: Float): Float = Math.sqrt(squareSum(x, y)).toFloat

  def squareSum(x: Float, y: Float): Double = x * x + y * y

  def entityDistance(e1: E, e2: E) = p2pDistance(e1, e2) - e1.radius - e2.radius

  def nextEntityDistance(e1: E, e2: E) = p2pDistance(nextPosition(e1, 5), nextPosition(e2, 5)) - e1.radius - e2.radius

  def biggerThan(e: E): (E) => Boolean = _.radius > e.radius

  def surface(entity: E) = surfaceWithRadius(entity.radius)

  def surfaceWithRadius(radius: Float): Float = (Math.PI * radius * radius / 2f).toFloat

  def propel(entity: E, direction: V) = {
    val speed = Vector2(entity.vx, entity.vy) - direction.normalize.scale(relativeSpeed).scale(1f / 14)
    Entity(entity.id, entity.playerId, bigRadius * entity.radius, entity.x, entity.y, speed.x, speed.y)
  }

  def computeDropletSpeed(propelled: E, e: E) = Vector2(e.vx, e.vy).scale(15) - Vector2(propelled.vx, propelled.vy).scale(14)

  def computeDroplet(propelled: E, entity: E) = {
    val dropletSpeed = computeDropletSpeed(propelled, entity)
    val dropletRadius: Float = smallRadius * entity.radius
    val projected = dropletSpeed.normalize.scale(propelled.radius - dropletRadius)
    Entity(-entity.id, -1, dropletRadius, entity.x + projected.x, entity.y + projected.y, dropletSpeed.x, dropletSpeed.y)
  }

  def positionWithDirection(e: E, direction: V): Point = {
    Point(e.x + direction.x, e.y + direction.y)
  }

  def propelCoor(propelled: E, e: E) = {
    val direction = computeDropletSpeed(propelled, e).normalize.scale(e.radius)
    positionWithDirection(e, direction.negate)
  }

  def mirrorX(e: E) = if (touchBorder(e.x + e.vx, e.radius, width))
    Entity(e.id, e.playerId, e.radius, if (e.x + e.vx < 0) -e.x + 2 * e.radius else 2 * width - e.x - 2 * e.radius, e.y, -e.vx, e.vy)
  else e

  def mirrorY(e: E) = if (touchBorder(e.y + e.vy, e.radius, height))
    Entity(e.id, e.playerId, e.radius, e.x, if (e.y + e.vy < 0) -e.y + 2 * e.radius else 2 * height - e.y - 2 * e.radius, e.vx, -e.vy)
  else e


  def collision(chaser: E, chasee: E, turns: Int = 1): Boolean = {
    if (turns <= 0) false
    else {
      val mirroredChaser = mirrorEntity(chaser)
      val mirroredChasee = mirrorEntity(chasee)
      if (collisionInOneTurn(mirroredChaser, mirroredChasee)) true
      else {
        collision(nextPosition(mirroredChaser), nextPosition(mirroredChasee), turns - 1)
      }
    }
  }

  def mirrorEntity(chaser: E): E = {
    mirrorX(mirrorY(chaser))
  }

  def collisionInOneTurn(chaser: E, chasee: E): Boolean = {
    collisionTime(chaser, chasee) match {
      case Some(time) => time <= 1
      case None => false
    }
  }

  def collisionTime(chaser: E, chasee: E): Option[Float] = {
    val chaserSpeed: V = relativeChaserSpeed(chaser, chasee)
    val e2eVector = e2eDistVector(chaser, chasee)
    if (zeroVector.equals(chaserSpeed) || zeroVector.equals(e2eVector)) {
      return None
    }
    val normalizedChaserSpeed = chaserSpeed.normalize
    val c2CProjectedOnMovingVector = normalizedChaserSpeed * e2eVector
    if (c2CProjectedOnMovingVector < 0) {
      return None
    }
    val closestDistToMoveVectorSquare = e2eVector.magnitudeSquare - c2CProjectedOnMovingVector * c2CProjectedOnMovingVector
    val sumRadiiSquared = Math.pow(chaser.radius + chasee.radius, 2)
    if (closestDistToMoveVectorSquare > sumRadiiSquared) {
      return None
    }

    val collisionPointToClosestPointSquared = sumRadiiSquared - closestDistToMoveVectorSquare
    val collisionDistance: Float = c2CProjectedOnMovingVector - Math.sqrt(collisionPointToClosestPointSquared).toFloat
    Some(collisionDistance / chaserSpeed.magnitude)
  }

  def relativeChaserSpeed(chaser: E, chasee: E): V = {
    Vector2(chaser.vx, chaser.vy) - Vector2(chasee.vx, chasee.vy)
  }

  def e2eDistVector(chaser: E, chasee: E): V = {
    Vector2(chaser, chasee)
  }

  def propelVector(chaser: E, chasee: E): Option[V] = {
    if (inCollisionScope(chaser, chasee)) Some(e2eDistVector(chaser, chasee)) else towardChasee(relativeChaserSpeed(chaser, chasee), e2eDistVector(chaser, chasee))
  }

  def inCollisionScope(chaser: E, chasee: E): Boolean = {
    collisionTime(chaser, chasee) match {
      case None => false
      case Some(time) => true
    }
  }

  def towardChasee(chaserSpeed: V, e2eVector: V): Option[V] = {
    val normalizedE2E = e2eVector.normalize
    val chaserProjectedOnE2ESpeed = normalizedE2E.scale(chaserSpeed * normalizedE2E)
    val chaserVertical = chaserSpeed - chaserProjectedOnE2ESpeed
    val acceleration: Float = relativeSpeed.abs / 14f
    if (chaserVertical.magnitude > acceleration) None
    else {
      val projectedMagnitude: Float = Math.sqrt(acceleration * acceleration - chaserVertical.magnitudeSquare).toFloat
      val propelProjected = normalizedE2E.scale(projectedMagnitude)
      Some(chaserVertical.negate + propelProjected)
    }
  }

  trait Action {
    def execute()

    def move(p: Point, message: String): Unit = println(p.x + " " + p.y + " " + message)
  }

  object WaitAction extends Action {
    override def execute(): Unit = println("WAIT Waiting")
  }

  case class ChaseAction(p: Point, chasee: E, message: String = "") extends Action {
    override def execute(): Unit = move(p, message + "->" + chasee.id)
  }

  case class DefenseAction(p: Point, defendee: E) extends Action {
    override def execute(): Unit = move(p, "<-" + defendee.id)
  }

  case class MoveAction(p: Point, message: String = "") extends Action {
    override def execute(): Unit = move(p, message)
  }

  trait Handler {
    def handle(entities: Es, entity: E): Option[Action]
  }

  def mirror(x: Float, r: Float, border: Float): Float = {
    if (x + r >= border) border - r - (x + r - border)
    else if (x - r <= 0) 2 * r - x else x
  }

  def touchBorder(x: Float, r: Float, border: Float) = {
    (x + r > border) || (x - r < 0)
  }

  def nextPosition(e: E, factor: Float = 1) = {
    val nx: Float = e.x + factor * e.vx
    val ny: Float = e.y + factor * e.vy
    val nvx = if (touchBorder(nx, e.radius, width)) -e.vx else e.vx
    val nvy = if (touchBorder(ny, e.radius, height)) -e.vy else e.vy
    Entity(e.id, e.playerId, e.radius, mirror(nx, e.radius, width), mirror(ny, e.radius, height), nvx, nvy)
  }

  object Defender extends Handler {
    val defenseMarge: Int = 2

    def escape(chaser: E, chasee: E): (Float, Float) = (chasee.x + (chasee.x - chaser.x) / chaser.radius, chasee.y + (chasee.y - chaser.y) / chaser.radius)

    override def handle(entities: Es, entity: E): Option[Action] = {
      entities.
        view.
        filter(_.playerId != entity.playerId).
        filter(biggerThan(entity)).find(e => {
        collision(entity, e, defenseMarge)
      }).flatMap(chaser => {
        Some(DefenseAction(Point(escape(chaser, entity)), chaser))
      })
    }
  }

  object Chaser extends Handler {
    override def handle(entities: Es, entity: E): Option[Action] = {
      if (entities.exists(collision(entity, _))) None
      else {
        val biggerOnesAttackMe = entities.view.map(e => if (e.radius > entity.radius && e.playerId != -1) propelVector(e, entity) match {
          case None => e
          case Some(v) => propel(e, v)
        } else e).map(e => mirrorEntity(e)).toList
        val propelledAnScores = allAngles.
          view.
          map(angle => {
          val propelled: Entity = propel(entity, angle)
          val droplet = computeDroplet(propelled, entity)
          (propelled, droplet)
        }).
          map({
          case (propelled, droplet) => (mirrorEntity(propelled), angleScore(propelled, mirrorEntity(droplet) :: biggerOnesAttackMe))
        })
        if (propelledAnScores.isEmpty) None
        else {
          val maxScore: (Entity, Float) = propelledAnScores.maxBy(_._2)
          if (maxScore._2 <= 0) None else Some(MoveAction(propelCoor(maxScore._1, entity), "chase"))
        }
      }
    }

    def angleScore(propelled: E, es: Es): Float = {
      es.flatMap(e => collisionTimeRegardingOtherChasers(propelled, e, es).map(time => score(propelled, e, time))).sum
    }

    def collisionTimeRegardingOtherChasers(chaser: E, chasee: E, entities: Es): Option[Float] = {
      collisionTime(chaser, chasee).flatMap(myTime => {
        if (entities.view.filter(notEntity(chaser)).filter(notEntity(chasee)).filter(_.radius > chasee.radius).exists(other => collisionTime(other, chasee) match {
          case None => false
          case Some(time) => time < myTime && surface(other) + surface(chasee) > surface(chaser)
        })) None
        else Some(myTime)
      })
    }

    def score(chaser: E, chasee: E, collisionTime: Float) = {
      val baseScore = chasee.radius / (1 + collisionTime)
      if (chasee.radius > chaser.radius && chasee.playerId != chaser.playerId) {
        -1f * baseScore
      } else if (chaser.radius != chasee.radius && chasee.playerId == chaser.playerId) {
        0.5f * baseScore
      } else if (chasee.radius < chaser.radius && chasee.playerId == -1) {
        2f * baseScore
      } else if (chasee.radius < chaser.radius && chasee.playerId != chaser.playerId) {
        1f * baseScore
      } else 0f
    }
  }


  def main(args: Array[String]) {
    play(readInt())
  }

  def readEntities: Es = {
    val playerChipCount = readInt() // The total number of entities on the table, including your chips
    (for (i <- 0 until readInt()) yield {
      val Array(id, player, radius, x, y, vx, vy) = readLine split " "
      Entity(id.toInt, player.toInt, radius.toFloat, x.toFloat, y.toFloat, vx.toFloat, vy.toFloat)
    }).toList
  }

  def play(playerId: Int): Unit = {
    val entities: Es = readEntities
    val actions = entitiesOfPlayer(entities, playerId).map(e => {
      (e, List(Defender, Chaser).view.flatMap(_.handle(entities, e)).headOption.getOrElse(WaitAction))
    })
    actions.foreach(_._2.execute())
    play(playerId)
  }

}
